# Ceainarie

Obiectivul acestui laborator a fost sa propunem, sa proiectam si sa implementam o aplicatie web. Eu am ales sa fac un site de ceaiuri in framework-ul Spring BOOT. Pe langa partea tehnica din spate, am creat o interfata grafica in HTML si CSS. 

# Studiul problemei
Find un framework pe care nu l-am mai utilizat, acest proiect a avut un grad destul de mare de dificultate pentru mine. Un alt principiu de care s-a tinut cont a fost implementarea si scrierea codului intr-o maniera prin care sa facilitam clasele parinte sa foloseasca metoda ale claselor copii, astefel ca "greutatea codului sa cada pe clase copii si nu parinte", fapt ce reduce considerabil numarul de erori dar si cantitatea de cod folosit in implementarea aplicatiei.




# Implmentare
Inified Modeling Language sau UML pe scurt este un limbaj standard pentru descrierea de modele si specipicatii pentru software. UML a fost la baza dezvoltat pentru reprezentarea complexitatii programelor orientate pe obiect, al caror fundament este structurarea programelor pe clase si instantele acestora (numite si obiecte). Cu toate acestea, datorita eficientei si claritaii in reprezentarea unor elemente abstracte, UML este utilizat dincolo de domeniul IT.

![N|Solidv](https://i.ibb.co/S0JFJtW/Capture.jpg)
![N|Solidv](https://i.ibb.co/nfjdQ6g/Capture1.jpg)
Dupa cum se poate vedea in clasele de service se efectueaza operatile de CRUD pe obiecte.
Am implemetat un observer care transmite un mesaj Administratorului cand nu mai exista un anumit tip de ceai. 
![N|Solidv](https://i.ibb.co/bBYT0Tz/Capture2.jpg)
Am creat o funtie care afiseaza toate ceaiurile sau toti useriisau toate intrebariile:
![N|Solidv](https://i.ibb.co/VY74JX7/Capture4.jpg)
Se pot adauga ceaiuri in baza de date:
![N|Solidv](https://i.ibb.co/4M5MVb5/Capture5.jpg)

Interfata am implementat-o in HTML si css, iar conexiunea dintre front end si back end am realizat-o cu JQuery. Am incercat sa realizez o interfata cat mai minimalista deoarece, scopul aplicatiei este de a atrage clienti care sa cumpere ceaiuri cu placere.
![N|Solidv](https://i.ibb.co/qr2nYMB/Capture6.jpg)
![N|Solidv](https://i.ibb.co/FWNqHXv/Capture7.jpg)
Aceasta este pagina de creare a unui cont nou:
![N|Solidv](https://i.ibb.co/5j8p32W/Capture8.jpg)

# Inbunatatiri ulterioare
In cadrul aplicatiei as mai dori sa fac in viitorul apropiat un set de intrebari pentru user, in urma caruia sa-i pot recomanda un tip de ceai. In cadrul site-ului as mai putea vinde ceainice, cani pentru ceai si alte ustensile utile pentru prepararea ceaiului.

# Bibliografie
https://www.w3schools.com/html/html_tables.asp
https://www.quackit.com/css/tutorial/css_text.cfm
https://www.youtube.com/user/caveofprogramming/playlists







