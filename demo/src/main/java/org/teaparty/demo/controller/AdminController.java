package org.teaparty.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.teaparty.demo.model.entities.Role;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.model.entities.User;
import org.teaparty.demo.model.security.Hashing;
import org.teaparty.demo.observer.MyTopic;
import org.teaparty.demo.observer.MyTopicSubscriber;
import org.teaparty.demo.observer.Observer;
import org.teaparty.demo.repository.dao.TeaDAO;
import org.teaparty.demo.repository.service.TeaService;
import org.teaparty.demo.repository.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("adminPage")
public class AdminController {
    @Autowired
    private UserService userService;

    @Autowired
    private TeaService teaService;

    @Autowired
    private TeaDAO teaDAO;

    @RequestMapping(value = "/observer")
    public String message(@RequestParam("name") String name) {
        if(teaService.findByName(name).getCantitate()==0 ) {
            return userService.messageObserver(name);
        }
        return null;
    }

    @RequestMapping(value = "/login")
    public String showWelcomePage(@RequestParam String name, @RequestParam String password) {
        boolean isValidUser = userService.validateUser(name, Hashing.hashPassword(password));
        if (!isValidUser) {
            return "Invalid Credentials";
        }

        return "conectat";
    }

    /*
    @RequestMapping(value = "/getAll")
    public User indexAdmin(@RequestParam("role") Role role){
        return  userService.listAll();
    }*/

    @GetMapping(value = "/getAdmin")
    public User getByNameAdmin(@RequestParam("name") String name) {
        return userService.findByName(name);

    }

    @PutMapping(value = "/insert")
    public User insertUser(@RequestParam("email") String email, @RequestParam("password") String password,
                           @RequestParam("name") String name, @RequestParam("last name") String lastname,
                           @RequestParam("role") Role role) {
        User user = new User(email,password,name,lastname,role);
        userService.insert(user);
        return user;
    }
    @RequestMapping(value = "/update")
    public User updateUser(@RequestParam("email") String email, @RequestParam("password") String password,
                           @RequestParam("name") String name, @RequestParam("last name") String lastname,
                           @RequestParam("role") Role role){
        User user = new User(email,password,name,lastname,role);
        userService.updateUser(user);
        return user;
    }

    @GetMapping(value = "/getAllTea")
    public List<Tea> getAllTea(){
        return  teaService.findAllTea();
    }

    @GetMapping(value = "/getTea")
    public Tea getByName(@RequestParam("name") String name) {
        return teaService.findByName(name);
    }



    @PostMapping(value = "/insertTea", produces = "application/json")
    public Tea insertTea(@Valid @RequestBody Tea tea) {
        return teaDAO.save(tea);
    }


    @RequestMapping(value = "/updateTea")
    public Tea updateTeas(@RequestParam("name") String name, @RequestParam("honey") Boolean honey,
                          @RequestParam("herbs") List<String> herbs, @RequestParam("fruits") List<String> fruits,
                          @RequestParam("price") double price,@RequestParam("type") String type,@RequestParam("cantitate") int cantitate){
        Tea tea = new Tea(name, fruits, herbs, honey, price,type,cantitate);
        teaService.updateTea(tea);
        return tea;
    }

    @DeleteMapping("delete/{teaId}")
    public void deleteTea(@PathVariable(value = "teaId") Long id ) {
                teaDAO.delete(id);
                //return "A fost sters";

           // return "Nu exista acest tip de ceai";
    }



    @PostMapping("/save")
    public String save(Tea tea){
        teaDAO.save(tea);
        return "redirect:/";
    }


    @GetMapping("/findOne")
    @ResponseBody
    public Tea findOne(Long id){
        return teaDAO.findOne(id);

    }

}