package org.teaparty.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PagesController {

    @GetMapping("/admin/myAdminPage")
    public String getAdminPage() {
        return "/adminPage";
    }

    @GetMapping("/user/general/myUserPage")
    public String getUserPage() {
        return "/userPage";
    }


    @GetMapping("/guest/myGuestPage")
    public String getGuestPage() {
        return "/guestsPage";
    }

}
