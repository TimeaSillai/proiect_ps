package org.teaparty.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.teaparty.demo.model.entities.Answer;
import org.teaparty.demo.model.entities.Content;
import org.teaparty.demo.model.entities.Question;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.repository.service.QuestionService;

import java.util.List;

@RestController
public class QuestionController {


    @Autowired
    private QuestionService questionService;

    @RequestMapping(value = "/getAll")
    public Iterable<Question> index(){
        return  questionService.findAllTea();
    }

    @GetMapping(value = "/get")
    public Question getByName(@RequestParam("name") String name) {
        return questionService.findByName(name);
    }

    @PutMapping(value = "/insert")
    public Question insertQuestion(@RequestParam("text") String text, @RequestParam("answer") Answer answers,
                                   @RequestParam("content") Content contents) {
        Question question = new Question(text,answers,contents);
        questionService.insert(question);
        return question;
    }
    @RequestMapping(value = "/update")
    public Question updateQuestion(@RequestParam("text") String text, @RequestParam("answer") Answer answers,
                         @RequestParam("content") Content contents) {
        Question question = new Question(text,answers,contents);
        questionService.update(question);
        return question;
    }





}
