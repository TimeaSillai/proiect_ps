package org.teaparty.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.repository.TeaFactory;
import org.teaparty.demo.repository.dao.TeaDAO;
import org.teaparty.demo.repository.service.TeaService;

import java.util.List;


// nu ai voie sa ai tea normal ca si un request mapping . request mapping se refera la cum trebuie sa arate URL in browser
//valid ar fi sa pui /tea
//nu mapa controller-ul . mapeaza fiecare metoda cu value = link-ul unde vrei sa te duci
@RestController

@RequestMapping("tea")
public class TeaController {

    @Autowired
    private TeaFactory teaFactory;
   @Autowired
   private TeaService teaService;

    //@Autowired
    //private TeaDAO teaDAO;

    @RequestMapping(value = "/getAll")
    public Iterable<Tea> index(){
        return  teaService.findAllTea();
    }

    @GetMapping(value = "/get")
    public Tea getByName(@RequestParam("name") String name) {
        return teaService.findByName(name);
    }

    @PutMapping(value = "/insert")
    public Tea insertTea(@RequestParam("name") String name, @RequestParam("honey") Boolean honey,
                         @RequestParam("herbs") List<String> herbs, @RequestParam("fruits") List<String> fruits,
                         @RequestParam("price") double price, @RequestParam("type") String type, @RequestParam("cantitate") int cantitate ) {
        Tea tea = new Tea(name, fruits, herbs, honey, price,type,cantitate);
        teaService.insert(tea);
        return tea;
    }
    @RequestMapping(value = "/update")
    public Tea updateTeas(@RequestParam("name") String name, @RequestParam("honey") Boolean honey,
                         @RequestParam("herbs") List<String> herbs, @RequestParam("fruits") List<String> fruits,
                         @RequestParam("price") double price,@RequestParam("type") String type,@RequestParam("cantitate") int cantitate){
        Tea tea = new Tea(name, fruits, herbs, honey, price,type,cantitate);
        teaService.updateTea(tea);
        return tea;
    }


}
