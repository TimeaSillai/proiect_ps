package org.teaparty.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.teaparty.demo.model.entities.Role;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.model.entities.User;
import org.teaparty.demo.model.security.Hashing;
import org.teaparty.demo.observer.Observer;
import org.teaparty.demo.repository.service.ServiceResponse;
import org.teaparty.demo.repository.service.TeaService;
import org.teaparty.demo.repository.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private TeaService teaService;

    List<Tea> teaList = new ArrayList<>();

    @RequestMapping(value = "/getAll")
    public Iterable<User> index(){
        return  userService.findAllUser();
    }

    @GetMapping(value = "/get")
    public User getByName(@RequestParam("name") String name) {
        return userService.findByName(name);

    }

    @PutMapping(value = "/insert")
    public User insertUser(@RequestParam("email") String email, @RequestParam("password") String password,
                         @RequestParam("name") String name, @RequestParam("last name") String lastname,
                           @RequestParam("role") Role role) {
        User user = new User(email,password,name,lastname,role);
        userService.insert(user);
        return user;
    }
    @RequestMapping(value = "/update")
    public User updateUser(@RequestParam("email") String email, @RequestParam("password") String password,
                           @RequestParam("name") String name, @RequestParam("last name") String lastname,
                           @RequestParam("role") Role role){
        User user = new User(email,password,name,lastname,role);
        userService.updateUser(user);
        return user;
    }

    @RequestMapping(value = "/login")
    public String showWelcomePage(@RequestParam String name, @RequestParam String password) {
        boolean isValidUser = userService.validateUser(name, Hashing.hashPassword(password));
        if (!isValidUser) {
            return "Invalid Credentials";
        }

        return "conectat";
    }

    @RequestMapping(value = "/updateTea")
    public Tea updateTeas(@RequestParam("name") String name, @RequestParam("honey") Boolean honey,
                          @RequestParam("herbs") List<String> herbs, @RequestParam("fruits") List<String> fruits,
                          @RequestParam("price") double price,@RequestParam("type") String type,@RequestParam("cantitate") int cantitate){
        Tea tea = new Tea(name, fruits, herbs, honey, price,type,cantitate);
        teaService.updateTea(tea);
        return tea;
    }

    @GetMapping(value = "/getTea")
    public Tea getByNameTea(@RequestParam("name") String name) {
        return teaService.findByName(name);
    }

    @RequestMapping(value = "/getAllTea")
    public ResponseEntity<Object> indexTea(){
        ServiceResponse<List<Tea>> response = new ServiceResponse<>("success", teaList);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @RequestMapping(value= "/buyTea")
    public Tea buyTea(@RequestParam("name") String name,@RequestParam("cantitate") int cantitate)
    {
        Tea tea=teaService.findByName(name);
        teaService.buy(tea,cantitate);
        return tea;
    }


}
