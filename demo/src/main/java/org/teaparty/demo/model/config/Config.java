package org.teaparty.demo.model.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.teaparty.demo.repository.TeaFactory;
import org.teaparty.demo.repository.service.QuestionService;
import org.teaparty.demo.repository.service.TeaService;
import org.teaparty.demo.repository.service.UserService;

@Configuration // declari bean-urile ( aici spui cine sa mearga in valoarea variabilelor care au adnotatia @Autowired);
public class Config {
    @Bean
    public TeaService getTeaService(){  //in controller avem @Autowired pentru TeaService. undeva trebuie sa creeam o instanta de tea service sa i-o dam
        //asta se intampla aici
        return new TeaService();
    }
    @Bean
    public TeaFactory getTeaFactory(){ // idem ca si tea service
        return new TeaFactory();
    }

    @Bean
    public QuestionService getQuestionService(){
        return  new QuestionService();
    }

    @Bean
    public UserService getUserService(){
        return  new UserService();
    }
}
