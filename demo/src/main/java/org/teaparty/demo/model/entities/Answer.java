package org.teaparty.demo.model.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "answer", schema = "tea-mv")
public class Answer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;

    @NotNull
    @Size(min = 3, max = 15)
    private String text;

    @OneToMany
    @JoinColumn(name = "answer_id")
    public List<Question> questions;

    public Answer(String text) {
        this.text = text;
    }

    public Answer() {}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}


