package org.teaparty.demo.model.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "content", schema = "tea-mv")
public class Content {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;

    @NotNull
    @Size(min = 3, max = 15)
    private String text;

    @OneToMany
    @JoinColumn(name = "content_id")
    public List<Question> questions;

    public Content(String text) {
        this.text = text;
    }

    public Content() {}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}


