package org.teaparty.demo.model.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;

    @NotNull
    @Size(min = 3, max = 15)
    public String text;

    @ManyToOne
    public Answer answer;

    @ManyToOne
    public Content content;

    public Question(String text, Answer answer, Content content) {
        this.text = text;
        this.answer = answer;
        this.content = content;
    }

    public Question()
    {}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }
}
