package org.teaparty.demo.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.jackson.JsonComponent;
import org.teaparty.demo.model.seed.TeaDeserializer;
import org.teaparty.demo.repository.TeaFactory;

import javax.annotation.sql.DataSourceDefinition;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="tea")
@JsonComponent
public class Tea implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    // id-ul e auto generat de aia nu il punem in constructor
    @Column(name="name")
    private String name;    //nume ceai

    @ElementCollection(targetClass = String.class)
    @Column(name="fruits")
    private List<String> fruits;    //chestia asta face o relatie 1 to many cu o tabela care va contine id-ul ceailui intr-o coloana si in cealalta un fruct
    //asa se mapeaza lista de fructe intr-o alta tabela( contine stringuri)

    @ElementCollection(targetClass = String.class)
    @Column(name="herbs")
    private List<String> herbs; // idem ca si fructele

    @NotNull
    @Column(name="honey")
    private boolean honey;

    @NotNull
    @Column(name="price")
    private double price;

    @NotNull
    @Column(name="teaType")
    private String teaType;

    @NotNull
    @Column(name="cantitate")
    private int cantitate;

    public Tea(String name, List<String> fruits, List<String> herbs, boolean honey, double price,String teaType, int cantitate) {
        this.name = name;
        this.fruits = fruits;
        this.herbs = herbs;
        this.honey = honey;
        this. price= price;
        this.teaType= teaType;
        this.cantitate=cantitate;

    }

    public Tea(){

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getFruits() {
        return fruits;
    }

    public void setFruits(List<String> fruits) {
        this.fruits = fruits;
    }

    public List<String> getHerbs() {
        return herbs;
    }

    public void setHerbs(List<String> herbs) {
        this.herbs = herbs;
    }

    public boolean isHoney() {
        return honey;
    }

    public void setHoney(boolean honey) {
        this.honey = honey;
    }

    public double getPrice() {return price; }

    public void setPrice(double price) { this.price = price; }

    public int getCantitate() { return cantitate; }

    public void setCantitate(int cantitate) { this.cantitate = cantitate; }

    public String getTeaType() {
        return teaType;
    }

    public void setTeaType(String type) {
        this.teaType = TeaFactory.getTea(type).type();
    }

    public int compareTo(Tea tea){
        return this.price > tea.price ? 1 : this.price < tea.price ? -1 : 0;
    }

}
