package org.teaparty.demo.model.factory;

public interface TeaType {
    public String type();
}
