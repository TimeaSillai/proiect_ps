package org.teaparty.demo.model.seed;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import org.teaparty.demo.model.entities.Tea;

import java.io.IOException;
import java.util.List;

public class TeaDeserializer  extends StdDeserializer<Tea> {

    public TeaDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Tea deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        //Long id = (Long) ( node.get("id")).numberValue();
        String itemName = node.get("name").asText();
        String fruits = node.get("fruits").asText();
        String herbs = node.get("herbs").asText();
        boolean honey = Boolean.parseBoolean(node.get("honey").asText());
        double price = (double)(node.get("price").numberValue());
        String teaType = node.get("teaType").asText();
        int cantitate = (Integer) (node.get("cantitate").numberValue());

        List<String> list = null;
        list.add(fruits);

        List<String> list2 = null;
        list.add(herbs);
        return new Tea(itemName, list, list2,honey,price,teaType,cantitate);
    }
}
