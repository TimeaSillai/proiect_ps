package org.teaparty.demo.observer;

public class MyTopicSubscriber implements Observer {

    private String name;
    private Subject topic;

    public MyTopicSubscriber(String nm){
        this.name=nm;
    }
    @Override
    public String update() {
        String msg = (String) topic.getUpdate(this);
        if(msg == null){
            return name+":: No new message";
        }else
            return name+":: Consuming message::"+msg;
    }

    @Override
    public void setSubject(Subject sub) {
        this.topic=sub;
    }

}
