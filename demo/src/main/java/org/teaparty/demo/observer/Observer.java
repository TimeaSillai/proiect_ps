package org.teaparty.demo.observer;

public interface Observer {

    //method to update the observer, used by subject
    public String update();

    //attach with subject to observe
    public void setSubject(Subject sub);
}
