package org.teaparty.demo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.teaparty.demo.model.factory.BlackTea;
import org.teaparty.demo.model.factory.GreenTea;
import org.teaparty.demo.model.factory.TeaType;
import org.teaparty.demo.model.factory.WhiteTea;
import org.teaparty.demo.repository.service.TeaService;

public class TeaFactory {

    @Autowired
    private TeaService teaService;

    /**
     * dai numele tipului de ceai si iti returneaza primu ceai din db care corespunde cu numele ala, un fel de "factory"
     * @param name
     * @return
     */
    public static TeaType getTea(String name){
        if(name.equals("black")){
            return new BlackTea();
        }else if(name.equals("green")){
            return new GreenTea();
        }else if(name.equals("white")){
            return new WhiteTea();
        }

        return null; // default daca nu da ce trebe

    }

}
