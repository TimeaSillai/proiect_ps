package org.teaparty.demo.repository.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.Answer;
import org.teaparty.demo.model.entities.Question;
import org.teaparty.demo.model.entities.Tea;

import javax.transaction.Transactional;
import java.util.List;
@Repository
@Transactional
public interface AnswerDAO extends CrudRepository<Answer,Long> {

}
