package org.teaparty.demo.repository.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.Content;
import org.teaparty.demo.model.entities.Question;

import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public interface ContentDAO extends CrudRepository<Content,Long> {

}
