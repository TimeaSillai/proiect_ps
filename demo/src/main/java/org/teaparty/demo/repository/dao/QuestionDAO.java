package org.teaparty.demo.repository.dao;
import org.springframework.data.repository.CrudRepository;
import org.teaparty.demo.model.entities.Question;
import java.util.List;

public interface QuestionDAO extends CrudRepository<Question,Long> {

    List<Question> findAllByText(final String text);
}
