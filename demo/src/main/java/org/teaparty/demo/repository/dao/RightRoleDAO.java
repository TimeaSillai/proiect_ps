package org.teaparty.demo.repository.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.RightRole;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RightRoleDAO extends CrudRepository<RightRole, Integer> {
}
