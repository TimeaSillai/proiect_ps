package org.teaparty.demo.repository.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.Role;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RoleDAO extends CrudRepository<Role, Integer> {
}

