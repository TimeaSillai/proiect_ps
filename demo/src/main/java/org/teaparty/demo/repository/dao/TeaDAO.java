package org.teaparty.demo.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.Tea;

import javax.transaction.Transactional;
import java.util.List;
@Repository
public interface TeaDAO extends JpaRepository<Tea, Long> {
    List<Tea> findAllByName(final String name);


}
