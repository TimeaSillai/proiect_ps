package org.teaparty.demo.repository.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.teaparty.demo.model.entities.Role;
import org.teaparty.demo.model.entities.User;

import javax.transaction.Transactional;
import java.util.List;
@Repository
@Transactional
public interface UserDAO extends JpaRepository<User, Long> {
    List<User> findAllByName(final String name);
    List<User> findAllByRole(final Role role);
}
