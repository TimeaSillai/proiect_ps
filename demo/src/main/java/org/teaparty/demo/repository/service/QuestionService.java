package org.teaparty.demo.repository.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teaparty.demo.model.entities.Content;
import org.teaparty.demo.model.entities.Question;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.repository.dao.QuestionDAO;
import org.teaparty.demo.repository.dao.TeaDAO;

@Service
public class QuestionService { // tot ce face clasa asta e sa fie un layer in plus intre dao si controller. e folositoare pentru a face logica(verificari) plus testing

    @Autowired
    public QuestionDAO questionDAO;

    public Question findById(Long id){
        return questionDAO.findOne(id);
    }

    public boolean delete(Long id){
        if(questionDAO.exists(id)){
            questionDAO.delete(id);
            return true;
        }else{
            return false;
        }
    }

    public boolean insert( Question question){
        questionDAO.save(question);
        return true;
    }


    public Question findByName(String text){
        return questionDAO.findAllByText(text).get(0);
    }

    public Iterable<Question> findAllTea(){
        return  questionDAO.findAll();
    }

    /**
     *face update unui quiz in funtie de id-ul acestuia
     * @param question
     * @return
     */

    public Question update(Question question){
        Question newQuestion = findById(question.getId());
        mergeQuestion(newQuestion, question);
        return questionDAO.save(newQuestion);
    }

    /**
     * seteaza fiecare obiect al quizului caruia vrem sa-i facem update cu noul quiz pe care il dam
     * @param currentQuestion
     * @param newQuestion
     */
    private void mergeQuestion(Question currentQuestion, Question newQuestion) {
        currentQuestion.setText(newQuestion.getText());
        currentQuestion.setAnswer(newQuestion.getAnswer());
        currentQuestion.setContent(newQuestion.getContent());

    }

   // public Content quiz(Question question){
     //   Question newQuestion = findById(question.getId());
    //    Content newContent=

    //}


}


