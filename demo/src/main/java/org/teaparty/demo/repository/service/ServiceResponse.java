package org.teaparty.demo.repository.service;

import groovy.transform.ToString;

@ToString
public class ServiceResponse<T> {

    private String status;
    private T data;

    public ServiceResponse(String status, T data) {
        this.status = status;
        this.data = data;
    }
}

