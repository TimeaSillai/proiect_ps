package org.teaparty.demo.repository.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.observer.MyTopic;
import org.teaparty.demo.observer.MyTopicSubscriber;
import org.teaparty.demo.observer.Observer;
import org.teaparty.demo.repository.dao.TeaDAO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeaService { // tot ce face clasa asta e sa fie un layer in plus intre dao si controller. e folositoare pentru a face logica(verificari) plus testing

    @Autowired
    public TeaDAO teaDAO;


    @Transactional
    public List<Tea> findAllTea(){
        return teaDAO.findAll();
    }

    @Transactional
    public Tea insert(Tea tea){
        return teaDAO.save(tea);
    }

    public Tea findById(Long id){
        return teaDAO.findOne(id);
    }

    public boolean delete(Long id){
        if(teaDAO.exists(id)){
            teaDAO.delete(id);
            return true;
        }else{
            return false;
        }
    }



    public Tea findByName(String name){ return teaDAO.findAllByName(name).get(0);
    }



    /**
     * face update unui ceai in funtie de id-ul acestuia
     * @param tea
     * @return
     */
    public Tea updateTea(Tea tea){
        Tea tea1 = findById(tea.getId());
        mergeTea(tea1, tea);
        return teaDAO.save(tea1);
    }

    /**
     * seteaza fiecare obiect al ceaiului caruia vrem sa-i facem update cu noul ceai pe care il dam
     * @param currentTea
     * @param newTea
     */
    private void mergeTea(Tea currentTea, Tea newTea) {
        currentTea.setName(newTea.getName());
        currentTea.setFruits(newTea.getFruits());
        currentTea.setHerbs(newTea.getHerbs());
        currentTea.setHoney(newTea.isHoney());
        currentTea.setCantitate(newTea.getCantitate());
        //currentTea.setPrice(newTea.getPrice());
        currentTea.setTeaType(newTea.getTeaType());
    }

    public Tea update(Tea currentTea, Tea newTea){
        currentTea.setName(newTea.getName());
        currentTea.setFruits(newTea.getFruits());
        currentTea.setHerbs(newTea.getHerbs());
        currentTea.setHoney(newTea.isHoney());
        currentTea.setCantitate(newTea.getCantitate());
        currentTea.setPrice(newTea.getPrice());
        //currentTea.setTeaType(newTea.getTeaType());
        return teaDAO.save(currentTea);
    }

    public boolean verificaCantitate(String name){
        Tea tea = new Tea();
        tea = findByName(name);
        if(tea.getCantitate()==0){
            return true;
        }
        return false;

    }

    public Tea buy(Tea tea,int cantitate){
        Tea tea1 = new Tea();
        update(tea1,tea);
        if(tea.getCantitate()-cantitate>0) {
            tea1.setCantitate(tea1.getCantitate() - cantitate);
            update(tea1,tea);
            return tea1;
        }else if(tea1.getCantitate()-cantitate==0) {
            delete(tea1.getId());
        }else if(tea1.getCantitate()-cantitate<0)
        {
            getMessage("nu mai sunt ceaiuri");
        }

        return tea;
    }


    public String getMessage(String name){
        MyTopic topic = new MyTopic();
        Observer obj1 = new MyTopicSubscriber("Obj1");
        topic.register(obj1);
        obj1.setSubject(topic);
        obj1.update();
        return topic.postMessage("New Message");
    }
    /*
    public Iterable<Tea> sortByPrice(){
        List<Tea> teas = new ArrayList<Tea>();
        teas.add(teaDAO.findAll())
        Collections.sort(teaDAO.findAll());
        return teaDAO.findAll();
    }
    */


}
