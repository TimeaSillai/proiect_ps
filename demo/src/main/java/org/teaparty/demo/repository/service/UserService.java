package org.teaparty.demo.repository.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.teaparty.demo.model.entities.Role;
import org.teaparty.demo.model.entities.User;
import org.teaparty.demo.observer.MyTopic;
import org.teaparty.demo.observer.MyTopicSubscriber;
import org.teaparty.demo.observer.Observer;
import org.teaparty.demo.repository.dao.UserDAO;

import java.util.List;


@Service
@RequiredArgsConstructor
public class UserService {

    @Autowired
    public UserDAO userDAO;

    @Transactional
    public List<User> listAll(){return userDAO.findAll();}

    @Transactional
    public User insert(User user){ return userDAO.save(user);  }

    public User findById(Long id){ return userDAO.findOne(id);}

    public boolean delete(Long id){
        if(userDAO.exists(id)){
            userDAO.delete(id);
            return true;
        }else{
            return false;
        }
    }

    public Long findIdByname(String name){
        User user = new User();
        user = findByName(name);
        return user.getId();
    }



    public User findByName(String name){
        return userDAO.findAllByName(name).get(0);
    }




    public Iterable<User> findAllUser(){
        return userDAO.findAll();
    }

    /**
     * face update unui user in funtie de id-ul acestuia
     * @param user
     * @return
     */
    public User updateUser(User user){
        User user1 = findById((long) user.getId());
        mergeUser(user1, user);
        return userDAO.save(user1);
    }

    /**
     * seteaza fiecare obiect al user-ului caruia vrem sa-i facem update cu noul ceai pe care il dam
     * @param currentUser
     * @param newUser
     */
    private void mergeUser(User currentUser, User newUser) {

        currentUser.setEmail(newUser.getEmail());
        currentUser.setPassword(newUser.getPassword());
        currentUser.setName(newUser.getName());
        currentUser.setLastName(newUser.getLastName());
        currentUser.setRole(newUser.getRole());
    }
    public boolean validateUser(String name, String password){
        if(findByName(name).getName()==name){
            return true;
        }
        return false;
    }


    public String messageObserver(String name){
            MyTopic topic = new MyTopic();
            Observer obj1 = new MyTopicSubscriber("Obj1");
            topic.register(obj1);
            obj1.setSubject(topic);
            obj1.update();
            return topic.postMessage("New Message");
    }



}
