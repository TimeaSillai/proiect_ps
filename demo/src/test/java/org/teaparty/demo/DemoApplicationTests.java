package org.teaparty.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.teaparty.demo.observer.MyTopic;
import org.teaparty.demo.observer.MyTopicSubscriber;
import org.teaparty.demo.observer.Observer;



@SpringBootTest
public class DemoApplicationTests {


    @Test
    public void testObserver() {
        MyTopic topic = new MyTopic();
        Observer obj1 = new MyTopicSubscriber("Tea");
        topic.register(obj1);
        obj1.setSubject(topic);
        //obj1.update();
        topic.postMessage("Nu mai sunt ceaiuri");
        //System.out.println(topic.postMessage("Nu mai sunt ceaiuri"));
        assert(obj1.update().equals("Tea:: Consuming message::Nu mai sunt ceaiuri"));
    }
}

