package org.teaparty.demo;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.repository.dao.TeaDAO;
import org.teaparty.demo.repository.service.TeaService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;


public class TeaServiceTest {

    @Mock
    private TeaDAO teaDAO;

    @InjectMocks
    private TeaService teaService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testService(){
        Tea teaPentruTest = new Tea();
        teaPentruTest.setName("black");
        List<Tea> rezultatAsteptat = new ArrayList<>(); // creez ce ar trebui sa returneze dao
        rezultatAsteptat.add(teaPentruTest);//adaugam ce am vrea sa returneze
        when(teaDAO.findAllByName("black")).thenReturn(rezultatAsteptat); //specifici cum sa se comporte dao la apel
        assert(teaService.findByName("black").getName().equals("black")); // apelezi service care apeleaza dao si vezi daca ceea ce a dat e ceea ce aiz is tu
    }

    @Test
    public void testType(){
        Tea tea = new Tea();
        tea.setName("Irish");
        tea.setTeaType("black");
        assert(tea.getTeaType().equals("Black Tea: "));
    }

    @Test
    public void testFinById(){
        Tea tea = new Tea();
        tea.setName("irish");
        when(teaDAO.findOne((long) 1)).thenReturn(tea);
        assert(teaService.findById((long) 1).getName().equals("irish"));

    }

    @Test
    public void testBuy(){
        Tea tea = new Tea();
        tea.setName("irish");
        tea.setTeaType("black");
        tea.setCantitate(12);
        teaService.buy(tea,2);
        System.out.println(tea.getCantitate());
        List<Tea> rezultatAsteptat = new ArrayList<>(); // creez ce ar trebui sa returneze dao
        rezultatAsteptat.add(tea);//adaugam ce am vrea sa returneze
        when(teaDAO.findAllByName("irish")).thenReturn(rezultatAsteptat); //specifici cum sa se comporte dao la apel
        assert(teaService.findByName("irish").getCantitate()==10);
    }

    @Test
    public void testBuy1(){
        Tea tea = new Tea();
        tea.setName("irish");
        tea.setTeaType("black");
        tea.setCantitate(12);
        teaService.buy(tea,14);
        System.out.println(tea.getCantitate());
        List<Tea> rezultatAsteptat = new ArrayList<>(); // creez ce ar trebui sa returneze dao
        rezultatAsteptat.add(tea);//adaugam ce am vrea sa returneze
        when(teaDAO.findAllByName("irish")).thenReturn(rezultatAsteptat); //specifici cum sa se comporte dao la apel
        assert(teaService.findByName("irish").getCantitate()==-2);
    }


    @Test
    public void deleteTest(){
        String name = "irish";
        Tea tea = new Tea();
        tea.setName("irish");
        tea.setTeaType("black");
        tea.setCantitate(12);

        List<Tea> rezultatAsteptat = new ArrayList<>(); // creez ce ar trebui sa returneze dao
        rezultatAsteptat.add(tea);//adaugam ce am vrea sa returneze
        when(teaDAO.findAllByName("irish")).thenReturn(rezultatAsteptat); //specifici cum sa se comporte dao la apel
        Long id= teaService.findByName(name).getId();
        System.out.println(tea.getId());
        assert(teaService.delete(id) == false);
    }


}
