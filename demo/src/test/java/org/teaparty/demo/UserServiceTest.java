package org.teaparty.demo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.teaparty.demo.model.entities.Tea;
import org.teaparty.demo.model.entities.User;
import org.teaparty.demo.model.security.Hashing;
import org.teaparty.demo.repository.dao.UserDAO;
import org.teaparty.demo.repository.service.UserService;

import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.when;

public class UserServiceTest {
    @Mock
    private UserDAO userDAO;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void showWelcomePage(){
        String name="mihai";
        String parola="hai";
        User user= new User();
        user.setName("mihai");
        user.setPassword("hai");
        List<User> rezultatAsteptat = new ArrayList<>(); // creez ce ar trebui sa returneze dao
        rezultatAsteptat.add(user);//adaugam ce am vrea sa returneze
        when(userDAO.findAllByName("mihai")).thenReturn(rezultatAsteptat);
        boolean isValidUser = userService.validateUser(name, parola);
        assert (isValidUser==true);
    }



}
